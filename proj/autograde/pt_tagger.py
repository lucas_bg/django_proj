import pickle

import nltk


def tag_sentences(text):
    tagger = pickle.load(open('proj/autograde/tagger.pkl', 'rb'))
    portuguese_sent_tokenizer = nltk.data.load("tokenizers/punkt/portuguese.pickle")
    sentences = portuguese_sent_tokenizer.tokenize(text)
    return [tagger.tag(nltk.word_tokenize(sentence)) for sentence in sentences]


def tag_sentences_and_join(text):
    tagger = pickle.load(open('proj/autograde/tagger.pkl', 'rb'))
    portuguese_sent_tokenizer = nltk.data.load("tokenizers/punkt/portuguese.pickle")
    sentences = portuguese_sent_tokenizer.tokenize(text)
    r = [tagger.tag(nltk.word_tokenize(sentence)) for sentence in sentences]
    return [item for sublist in r for item in sublist]
