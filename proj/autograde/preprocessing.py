import re
import unicodedata

import hunspell
import nltk
from nltk.corpus import stopwords, wordnet

from .pt_tagger import tag_sentences, tag_sentences_and_join

PT_STOPWORDS = (set(stopwords.words('portuguese')) - {'não'}).union({'vc', 'etc'})
PT_NEG_WORDS = {'não', 'nenhum', 'jamais', 'nada', 'nunca', 'nem', 'ninguém', 'n', 'num'}


def from_text_to_word_list(raw_text):
    return nltk.word_tokenize(raw_text, language='portuguese')


def word_tokenize(raw_text):
    return nltk.word_tokenize(raw_text, language='portuguese')


def sentence_tokenize(raw_text):
    return nltk.sent_tokenize(raw_text, language='portuguese')


def from_word_list_to_text(word_list):
    return ' '.join(word_list)


def get_wordnet_pos(treebank_tag):
    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        return None


def stem_words(word_list):
    stemmer = nltk.stem.RSLPStemmer()
    return [stemmer.stem(word) for word in word_list]


def tag_words(raw_text):
    tagged_sentences = tag_sentences(raw_text)
    tagged_words = []
    for s in tagged_sentences:
        for w in s:
            tagged_words.append(w)

    return tagged_words


def tag_sentences_join(text):
    return tag_sentences_and_join(text)


def lowercase(raw_text):
    return raw_text.lower()


def strip_accents(raw_text):
    return ''.join(
        c for c in unicodedata.normalize('NFD', raw_text) if unicodedata.category(c) != 'Mn'
    )


def only_alpha_numeric(raw_text):
    text = re.sub('\s+', ' ', raw_text).strip()
    return re.sub('[^a-z0-9]', ' ', text)


def remove_stopwords(word_list):
    return [w for w in word_list if w not in set(PT_STOPWORDS)]


def remove_symbols(word_list):
    return [w for w in word_list if w.isalpha()]


def spelling_correction(word_list):
    n_wl = []
    h = hunspell.Hunspell('pt_BR')
    for w in word_list:
        if (not h.spell(w)) and len(w) > 3:
            n_wl.append(h.suggest(w)[0])
        else:
            n_wl.append(w)
    return n_wl


def spell(word):
    h = hunspell.Hunspell('pt_BR')
    if (not h.spell(word)) and len(word) > 3:
        return h.suggest(word)[0]
    else:
        return word


def preprocess(
    raw_text, st_or_lmm='st', lower=True, strip_acc=True,
    only_alphan=True, stopws=False, sc=False):
    text = raw_text

    if lower:
        text = lowercase(text)

    if strip_acc:
        text = strip_accents(text)

    if only_alphan:
        text = only_alpha_numeric(text)

    word_list = from_text_to_word_list(text)

    if st_or_lmm == 'st':
        word_list = stem_words(word_list)
    else:
        word_list = stem_words(word_list)

    if stopws:
        word_list = remove_stopwords(word_list)

    if sc:
        word_list = spelling_correction(word_list)

    return word_list


def remove_nao_sei_e_ponto(answer_list, grade_list):
    v = ['.', '..', '...', '....', 'nao sei', 'não sei', 'não sei.', 'nao sei.']
    n_answer_list = []
    n_grade_list = []
    for i in range(0, len(answer_list)):
        s = answer_list[i]
        s = s.lower()
        if not (any([s == e for e in v])):
            n_answer_list.append(answer_list[i])
            n_grade_list.append(grade_list[i])
    return n_answer_list, n_grade_list


def remove_duplicates(answer_list, grade_list):
    answer_set = set()
    n_answer_list = []
    n_grade_list = []
    for i in range(0, len(answer_list)):
        s = answer_list[i]
        if s not in answer_set:
            answer_set = answer_set.union({s})
            n_answer_list.append(answer_list[i])
            n_grade_list.append(grade_list[i])
    return n_answer_list, n_grade_list
