import numpy as np

from .lexical_similarity import get_lexical_similarity_values
from .text_statistics import all_stats, get_lda
from .wordnet_similarity import wordnet_similarity_scores_v3_all_mean_median
from . import preprocessing as pp


def preprocess_single(answer, tipo):
	if tipo == 'NGRAMS':
		options = dict(
			st_or_lmm='st',
			stopws=False,
			sc=False
		)
	elif tipo == 'WN':
		options = dict(
			st_or_lmm='lmm',
			acc=False,
			stopws=True,
			sc=False
		)
	else:
		options = None
	return pp.from_word_list_to_text(pp.preprocess(answer, options))


def get_ngrams(cv1, cv2, answers):
	# pp_answers = [preprocess_single(a, tipo='NGRAMSS') for a in answers]
	x1 = cv1.transform(answers).toarray()
	x2 = cv2.transform(answers).toarray()
	return np.concatenate((x1, x2), axis=1)


def get_lex_feats(a, ref_anss):
	row = []
	for ra in ref_anss:
		row.extend(get_lexical_similarity_values(a, ra))
	return np.array(row)


def get_ts_lda_feats(answers, x_bow):
	x_txs = np.array([all_stats(a) for a in answers])
	x_lda = get_lda(x_bow)
	# x_cpts = concepts_match(answers, q_id)
	# return np.concatenate((x_txs, x_cpts, x_lda), axis=1)
	return np.concatenate((x_txs, x_lda), axis=1)


def get_wn_feats(answer, ref_anss):
	# pp_a = preprocess_single(answer, tipo='WN')
	row = []
	for ra in ref_anss:
		row.extend(wordnet_similarity_scores_v3_all_mean_median(ra, answer))
	return np.array(row)
