from textdistance.algorithms.compression_based import bwtrle_ncd, zlib_ncd, bz2_ncd, lzma_ncd
from textdistance.algorithms.edit_based import Levenshtein, JaroWinkler, Hamming
from textdistance.algorithms.sequence_based import LCSStr, RatcliffObershelp
from textdistance.algorithms.simple import Length
from textdistance.algorithms.token_based import Cosine, Sorensen, Overlap


def get_lexical_similarity_values(string1, string2):
	return [
		Cosine().normalized_similarity(string1, string2),
		Sorensen().normalized_similarity(string1, string2),
		Overlap().normalized_similarity(string1, string2),
		Hamming().normalized_similarity(string1, string2),
		Levenshtein().normalized_similarity(string1, string2),
		JaroWinkler().normalized_similarity(string1, string2),
		LCSStr().normalized_similarity(string1, string2),
		RatcliffObershelp().normalized_similarity(string1, string2),
		Length().normalized_similarity(string1, string2),
		bwtrle_ncd.normalized_similarity(string1, string2),
		zlib_ncd.normalized_similarity(string1, string2),
		lzma_ncd.normalized_similarity(string1, string2),
		bz2_ncd.normalized_similarity(string1, string2),
	]
