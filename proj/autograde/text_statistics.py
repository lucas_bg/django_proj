from sklearn.decomposition import LatentDirichletAllocation

from . import preprocessing as pp


def spelling_errors_count(essay):
	words = pp.word_tokenize(essay)
	corrected = pp.spelling_correction(words)
	s = 0
	for i in range(0, len(words)):
		if words[i] != corrected[i]:
			s += 1
	return s


def character_count(essay):
	return len(essay)


def word_count(essay):
	return len(essay.split(' '))


def sentence_count(essay):
	c = 0
	if essay.endswith('.'):
		c -= 1
	c = 1 + essay.count('.') - essay.count('..') - essay.count('...')
	c = c - essay.count('....') - essay.count('.....') - essay.count('......')
	if c < 1:
		return 1
	return c


def comma_count(essay):
	return essay.count(',')


def unique_word_count(essay):
	tokens = pp.word_tokenize(essay)
	tokens = [t.lower() for t in tokens if t.isalpha()]
	return len(set(tokens))


def word_avg_length(essay):
	tokens = pp.word_tokenize(essay)
	tokens = [t for t in tokens if t.isalpha()]
	w_l = [len(w) for w in tokens]
	if len(w_l) != 0:
		return sum(w_l)/len(w_l)
	else:
		return 0


def words_per_sentence_avg(essay):
	w_p_s = [word_count(s) for s in pp.sentence_tokenize(essay)]
	if len(w_p_s) != 0:
		return sum(w_p_s)/len(w_p_s)
	else:
		return 0


def negation_count(essay):
	return sum([1 if w == nw else 0 for nw in pp.PT_NEG_WORDS for w in pp.word_tokenize(essay)])


def count_tag_in_tags(ts, tags):
	return sum([1 if ts == t else 0 for t in tags])


def tags_counts(essay):
	tagged = pp.tag_sentences_join(essay)
	tags = [tag for (word, tag) in tagged]
	tag_set = ['ADJ', 'ADP', 'ADV', 'CONJ', 'DET', 'NOUN', 'NUM', 'PRT', 'PRON', 'VERB', '.', 'X']
	return [count_tag_in_tags(ts, tags) for ts in tag_set]


all_funcs_names = [
	# spelling_errors_count,
	character_count,
	word_count,
	sentence_count,
	comma_count,
	unique_word_count,
	word_avg_length,
	words_per_sentence_avg,
	negation_count,
	# tags_counts,  # separated because returns list
]


def all_stats(essay):
	ll = [f(essay) for f in all_funcs_names]
	ll.extend(tags_counts(essay))
	return ll


def get_lda(x_bow):
	lda = LatentDirichletAllocation(
		n_components=10,
		max_iter=5,
		learning_method='online',
		learning_offset=50.,
		random_state=0).fit(x_bow)
	return lda.transform(x_bow)
