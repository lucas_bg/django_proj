import math
import pickle

import numpy as np
from nltk.corpus import mac_morpho
from nltk.corpus import wordnet as wn

from . import preprocessing as pp


def universal_to_wn_tag(u_tag):
    if u_tag == 'NOUN':
        return wn.NOUN
    elif u_tag == 'VERB':
        return wn.VERB
    elif u_tag == 'ADJ':
        return wn.ADJ
    elif u_tag == 'ADV':
        return wn.ADV
    else:
        return None


def cautious_mean(lt, median=False):
    if len(lt) > 0:
        with np.errstate(divide='ignore', invalid='ignore', over='ignore', under='ignore'):
            lt = [x for x in lt if x is not None]
            lt = [x for x in lt if 1e+10 > x > 1e-10]
            lt = np.array(lt, dtype=np.float)
            lt[lt == np.inf] = 0
            lt = np.nan_to_num(lt)
            if len(lt) > 0:
                if median:
                    m = np.median(lt)
                else:
                    m = np.mean(lt)
            else:
                m = 0
            if math.isnan(m) or m is np.inf or math.isinf(m) or m is np.nan:
                m = 0
            return m
    else:
        return 0


open_classes = {'NOUN', 'VERB', 'ADJ', 'ADV'}


def get_mac_morpho_ic():
    return wn.ic(mac_morpho)


def wordnet_similarity_scores_v3_all_mean_median(ref_ans, answer_text):
    ref_ans_tagged = pp.tag_words(ref_ans)
    ref_ans_tagged_s = [(w, t) for (w, t) in ref_ans_tagged if t in open_classes]

    lch_scores = []
    path_scores = []
    res_scores = []
    wup_scores = []
    lin_scores = []
    jcn_scores = []

    mac_morpho_ic = pickle.load(open('proj/autograde/objs/mac_morpho_obj.pkl', 'rb'))

    for (w, t) in ref_ans_tagged_s:
        answer_text_tagged = pp.tag_words(answer_text)
        answer_text_tagged_s = [(w1, t1) for (w1, t1) in answer_text_tagged if t1 in open_classes]

        ref_syn_l = wn.synsets(w, pos=universal_to_wn_tag(t), lang='por')
        for ref_syn in ref_syn_l:
            for (w_stu, t_stu) in answer_text_tagged_s:
                stu_syn_l = wn.synsets(w_stu, pos=universal_to_wn_tag(t_stu), lang='por')
                for stu_syn in stu_syn_l:
                    if stu_syn._pos == 's':
                        stu_syn._pos = 'n'
                    if ref_syn._pos == stu_syn._pos:
                        lch_scores.append(ref_syn.lch_similarity(stu_syn))
                        path_scores.append(ref_syn.path_similarity(stu_syn))
                        wup_scores.append(ref_syn.wup_similarity(stu_syn))
                        res_scores.append(ref_syn.res_similarity(stu_syn, mac_morpho_ic))
                        lin_scores.append(ref_syn.lin_similarity(stu_syn, mac_morpho_ic))
                        jcn_scores.append(ref_syn.jcn_similarity(stu_syn, mac_morpho_ic))

    return [
        cautious_mean(lch_scores),
        cautious_mean(path_scores),
        cautious_mean(wup_scores),
        cautious_mean(res_scores),
        cautious_mean(lin_scores),
        cautious_mean(jcn_scores),
        cautious_mean(lch_scores, True),
        cautious_mean(path_scores, True),
        cautious_mean(wup_scores, True),
        cautious_mean(res_scores, True),
        cautious_mean(lin_scores, True),
        cautious_mean(jcn_scores, True),
    ]
