from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^$',
        view=views.after_login_view,
        name='redirect'
    ),
    url(
        regex=r'^~redirect/$',
        view=views.after_login_view,
        name='redirect'
    ),
    url(
        regex=r'^(?P<username>[\w.@+-]+)/$',
        view=views.UserDetailView.as_view(),
        name='detail'
    ),
    url(
        regex=r'^~chooseusertype/$',
        view=views.choose_user_type,
        name='chooseusertype'
    ),
    url(
        regex=r'^~studentsignup/$',
        view=views.StudentCreateView.as_view(),
        name='studentsignup'
    ),
    url(
        regex=r'^~teachersignup/$',
        view=views.TeacherCreateView.as_view(),
        name='teachersignup'
    ),
    url(
        regex=r'^~studentupdate/$',
        view=views.StudentUpdateView.as_view(),
        name='studentupdate'
    ),
    url(
        regex=r'^~teacherupdate/$',
        view=views.TeacherUpdateView.as_view(),
        name='teacherupdate'
    ),
]
