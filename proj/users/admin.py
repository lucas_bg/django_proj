from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from .models import User, Teacher, Student


class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class MyUserCreationForm(UserCreationForm):
    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


@admin.register(User)
class MyUserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    fieldsets = (
                    ('User Profile', {'fields': (
                        'requestForProfessor', 'isProfessor', 'finished_signup'
                    )}),
                ) + AuthUserAdmin.fieldsets
    list_display = ('id', 'username', 'finished_signup', 'requestForProfessor', 'isProfessor')
    search_fields = ['username']
    list_filter = ['finished_signup', 'requestForProfessor', 'isProfessor']
    list_editable = ['isProfessor']


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):

    fields = ['user', 'nome', 'sobrenome', 'school_name']
    search_fields = ['nome', 'sobrenome']
    list_filter = ['school_name']
    list_display = ['user', 'nome', 'sobrenome']
    list_editable = ['nome', 'sobrenome']


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):

    fields = ['user', 'nome', 'sobrenome', 'school_name', 'serie']
    search_fields = ['nome', 'sobrenome']
    list_filter = ['school_name', 'serie']
    list_display = ['user', 'nome', 'sobrenome']
    list_editable = ['nome', 'sobrenome']
