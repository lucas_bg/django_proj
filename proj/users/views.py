from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.views.generic import DetailView, RedirectView, UpdateView, CreateView

from .models import User, Student, Teacher


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    slug_field = 'username'
    slug_url_kwarg = 'username'

    def get(self, request, *args, **kwargs):
        if request.user.username != self.kwargs['username']:
            messages.error(request, "Essa conta não te pertence!")
            return redirect(reverse('info-page'))
        else:
            return super().get(request, *args, **kwargs)


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})


@login_required
def choose_user_type(request):
    return render(request, 'chooseusertype.html')


@login_required
def home_view(request):
    if request.user.is_authenticated:
        if request.user.isProfessor:
            return redirect(reverse('t:teacherhome'))
        else:
            return redirect(reverse('s:studenthome'))
    else:
        return redirect(reverse('home'))


def error_400(request):
    return render(request, 'errors/400.html', {})


def error_403(request):
    return render(request, 'errors/403.html', {})


def error_404(request):
    return render(request, 'errors/404.html', {})


def error_500(request):
    return render(request, 'errors/500.html', {})


def error_view(request):
    return render(request, 'errors/error.html', {})


def info_view(request):
    return render(request, 'errors/info.html', {})


@login_required
def after_login_view(request):
    user = request.user
    if user.isProfessor:
        return redirect('/t/')
    else:
        return redirect('/s/')


class StudentCreateView(LoginRequiredMixin, CreateView):
    model = Student
    fields = ['nome', 'sobrenome', 'school_name', 'serie']

    def form_valid(self, form):
        messages.info(self.request, 'Seu cadastro foi realizado com sucesso!')
        form.instance.user = self.request.user
        return super(StudentCreateView, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, 'Ocorreu um erro no seu cadastro!')
        return super().form_invalid(form)

    def get_success_url(self):
        return reverse('home')


class TeacherCreateView(LoginRequiredMixin, CreateView):
    model = Teacher
    fields = ['nome', 'sobrenome', 'school_name']

    def form_valid(self, form):
        messages.info(self.request, 'Seu cadastro foi realizado com sucesso!')
        form.instance.user = self.request.user
        return super(TeacherCreateView, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, 'Ocorreu um erro no seu cadastro!')
        return super().form_invalid(form)

    def get_success_url(self):
        return reverse('home')


class StudentUpdateView(LoginRequiredMixin, UpdateView):
    fields = ['nome', 'sobrenome', 'school_name', 'serie']

    model = Student

    def form_valid(self, form):
        messages.info(self.request, 'Suas informações foram atualizadas com sucesso!')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, 'Ocorreu um erro na atualização das suas informações!')
        return super().form_invalid(form)

    def get_success_url(self):
        return reverse('home')

    # noinspection PyMethodOverriding
    def get_object(self):
        return Student.objects.get(user=self.request.user)


class TeacherUpdateView(LoginRequiredMixin, UpdateView):
    fields = ['nome', 'sobrenome', 'school_name']

    model = Teacher

    def form_valid(self, form):
        messages.info(self.request, 'Suas informações foram atualizadas com sucesso!')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, 'Ocorreu um erro na atualização das suas informações!')
        return super().form_invalid(form)

    def get_success_url(self):
        return reverse('home')

    # noinspection PyMethodOverriding
    def get_object(self):
        return Teacher.objects.get(user=self.request.user)
