from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class User(AbstractUser):
    name = models.CharField(_('Nomee'), blank=True, max_length=255)
    nome = models.CharField(_('Nome'), blank=True, max_length=50)
    sobrenome = models.CharField(_('Sobrenome'), blank=True, max_length=50)
    finished_signup = models.BooleanField(_('Terminou o cadastro'), default=False)
    isProfessor = models.BooleanField(_('Eh professor'), default=False)
    requestForProfessor = models.BooleanField(_('Quer ser prof'), default=False)

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})


class Teacher(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, related_name='ass_teacher')
    school_name = models.CharField(max_length=255, verbose_name='Nome da Escola')
    nome = models.CharField(_('Nome'), max_length=50)
    sobrenome = models.CharField(_('Sobrenome'), max_length=50)

    def __str__(self):
        return "{0} - {1} {2}".format(self.user.username, self.nome, self.sobrenome)

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.user.username})

    def save(self, *args, **kwargs):
        self.user.finished_signup = True
        self.user.requestForProfessor = True
        self.user.save()
        super(Teacher, self).save(*args, **kwargs)


class Student(models.Model):
    YEAR_IN_SCHOOL_CHOICES = (
        ('primeiro', 'Primeiro Ano'),
        ('segundo', 'Segundo Ano'),
        ('terceiro', 'Terceiro Ano'),
        ('quarto', 'Quarto Ano'),
        ('quinto', 'Quinto Ano'),
        ('sexto', 'Sexto Ano'),
        ('setimo', 'Setimo Ano'),
        ('oitavo', 'Oitavo Ano'),
        ('nono', 'Nono Ano'),
        ('primeiromedio', 'Primeiro Ano Ensino Medio'),
        ('segundomedio', 'Segundo Ano Ensino Medio'),
        ('terceiromedio', 'Terceiro Ano Ensino Medio')
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    serie = models.CharField(max_length=15, choices=YEAR_IN_SCHOOL_CHOICES, verbose_name='Série')
    school_name = models.CharField(max_length=255, verbose_name='Nome da Escola')
    nome = models.CharField(_('Nome'), max_length=50)
    sobrenome = models.CharField(_('Sobrenome'), max_length=50)

    def __str__(self):
        return "{0} {1}".format(self.nome, self.sobrenome)

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.user.username})

    def save(self, *args, **kwargs):
        self.user.finished_signup = True
        self.user.save()
        super(Student, self).save(*args, **kwargs)
