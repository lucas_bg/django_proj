from django.conf.urls import url
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    url(
        # /t/
        regex=r'^$',
        view=TemplateView.as_view(template_name='t/teacher_index.html'),
        name='teacherhome'
    ),
    url(
        # /t/test/new/
        regex=r'^test/new/$',
        view=views.TestCreateView.as_view(),
        name='test-new'
    ),
    url(
        # /t/test/update/(?P<pk>[0-9]+)/
        regex=r'^test/update/(?P<pk>[0-9]+)/$',
        view=views.TestUpdateView.as_view(),
        name='test-update'
    ),
    url(
        # /t/test/delete/(?P<pk>[0-9]+)/
        regex=r'^test/delete/(?P<pk>[0-9]+)/$',
        view=views.TestDeleteView.as_view(),
        name='test-delete'
    ),
    url(
        # /t/test/list/
        regex=r'^test/list/$',
        view=views.TestListView.as_view(),
        name='test-list'
    ),
    url(
        # /t/test/all/list/
        regex=r'^test/all/list/$',
        view=views.TestAllListView.as_view(),
        name='test-all-list'
    ),
    url(
        # /t/question/new/
        regex=r'^question/new/$',
        view=views.QuestionConceptRefAnsCreate.as_view(),
        name='question-new'
    ),
    url(
        # /t/question/update/(?P<pk>[0-9]+)/
        regex=r'^question/update/(?P<pk>[0-9]+)/$',
        view=views.QuestionConceptRefAnsUpdate.as_view(),
        name='question-update'
    ),
    url(
        # /t/question/listbytest/(?P<pk>[0-9]+)/
        regex=r'^question/listbytest/(?P<pk>[0-9]+)/$',
        view=views.QuestionByTestListView.as_view(),
        name='question-list-by-test'
    ),
    url(
        # /t/question/details/(?P<pk>[0-9]+)/
        regex=r'^question/details/(?P<pk>[0-9]+)/$',
        view=views.QuestionDetailView.as_view(),
        name='question-details'
    ),
    url(
        # /t/question/delete/(?P<pk>[0-9]+)/
        regex=r'^question/delete/(?P<pk>[0-9]+)/$',
        view=views.QuestionDeleteView.as_view(),
        name='question-delete'
    ),
    url(
        # /t/concept/add/(?P<pk>[0-9]+)/
        regex=r'^concept/add/(?P<pk>[0-9]+)/$',
        view=views.ConceptCreateView.as_view(),
        name='concept-add'
    ),
    url(
        # /t/refans/add/(?P<pk>[0-9]+)/
        regex=r'^refans/add/(?P<pk>[0-9]+)/$',
        view=views.RefAnsCreateView.as_view(),
        name='refans-add'
    ),
    url(
        # /t/question/answersgrading/(?P<pk>[0-9]+)/
        regex=r'^question/answersgrading/(?P<pk>[0-9]+)/$',
        view=views.question_answersgrading,
        name='question-answersgrading'
    ),
    url(
        # /t/question/(?P<pk>[0-9]+)/answers/
        regex=r'^question/(?P<pk>[0-9]+)/answers/$',
        view=views.AnswerListView.as_view(),
        name='answers-list'
    ),
]
