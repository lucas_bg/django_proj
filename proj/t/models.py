from django.db import models
from proj.users.models import User


class Question(models.Model):
    statement = models.TextField(verbose_name='Enunciado')
    test = models.ForeignKey('Test', on_delete=models.CASCADE, verbose_name='Prova', related_name='questions')
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='my_questions')

    def __str__(self):
        return self.statement


class Test(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='my_tests')
    name = models.CharField(max_length=255, verbose_name='Nome')
    description = models.CharField(max_length=255, verbose_name='Descrição')

    def __str__(self):
        return self.name


class ReferenceAnswer(models.Model):
    ref_ans_text = models.TextField(verbose_name='Resposta de Referência')
    question = models.ForeignKey('Question', on_delete=models.CASCADE, related_name='ref_answers')

    def __str__(self):
        return self.ref_ans_text


class Concept(models.Model):
    concept_text = models.CharField(max_length=255, verbose_name='Conceito')
    question = models.ForeignKey('Question', on_delete=models.CASCADE, related_name='concepts')

    def __str__(self):
        return self.concept_text


class Answer(models.Model):
    ZERO = 0
    UM = 1
    DOIS = 2
    TRES = 3
    GRADE_CHOICES = (
        (ZERO, '0'),
        (UM, '1'),
        (DOIS, '2'),
        (TRES, '3'),
    )

    user = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='my_answers')
    question = models.ForeignKey('Question', on_delete=models.CASCADE, related_name='answers')
    answer_text = models.TextField(verbose_name='Texto da Resposta')
    a_grade = models.IntegerField(verbose_name='Nota Automática', blank=True, null=True, choices=GRADE_CHOICES)
    a_bin_grade = models.NullBooleanField(verbose_name='Nota Binária Automática', blank=True, null=True)
    t_grade = models.IntegerField(verbose_name='Nota do Professor', blank=True, null=True, choices=GRADE_CHOICES)
    t_bin_grade = models.NullBooleanField(verbose_name='Nota Binária do Professor', blank=True, null=True)

    def __str__(self):
        return self.answer_text


class Grade(models.Model):
    ZERO = 0
    UM = 1
    DOIS = 2
    TRES = 3
    GRADE_CHOICES = (
        (ZERO, '0'),
        (UM, '1'),
        (DOIS, '2'),
        (TRES, '3'),
    )

    grader = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='grades_i_gave')
    answer = models.ForeignKey('Answer', on_delete=models.CASCADE, related_name='grades')
    ot_grade = models.IntegerField(verbose_name='Nota', choices=GRADE_CHOICES)
    ot_bin_grade = models.NullBooleanField(verbose_name='Nota Binária')

    def __str__(self):
        return str(self.ot_grade)
