from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView, DeleteView, DetailView

from proj.t import forms
from proj.t.forms import ConceptFormSet, ReferenceAnswerFormSet, QuestionForm, TestForm
from .models import Test, Question, Concept, ReferenceAnswer, Answer, Grade


class QuestionConceptRefAnsCreate(LoginRequiredMixin, CreateView):
    model = Question
    form_class = QuestionForm

    def get_form_kwargs(self):
        kwargs = super(QuestionConceptRefAnsCreate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_success_url(self):
        return reverse('t:teacherhome')

    def get_context_data(self, **kwargs):
        data = super(QuestionConceptRefAnsCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['concepts'] = ConceptFormSet(self.request.POST)
            data['refanswers'] = ReferenceAnswerFormSet(self.request.POST)
        else:
            data['concepts'] = ConceptFormSet()
            data['refanswers'] = ReferenceAnswerFormSet()
        return data

    def form_invalid(self, form):
        messages.error(self.request, 'Ocorreu um erro no cadastro!')
        return super().form_invalid(form)

    def form_valid(self, form):
        form.instance.user = self.request.user
        context = self.get_context_data()
        concepts = context['concepts']
        refanswers = context['refanswers']
        with transaction.atomic():
            # noinspection PyAttributeOutsideInit
            self.object = form.save()

            if refanswers.is_valid():
                refanswers.instance = self.object
                refanswers.save()
            if concepts.is_valid():
                concepts.instance = self.object
                concepts.save()
            messages.info(self.request, 'O cadastro da questão foi realizado com sucesso!')
        return super(QuestionConceptRefAnsCreate, self).form_valid(form)


class QuestionConceptRefAnsUpdate(LoginRequiredMixin, UpdateView):
    model = Question
    fields = ['statement', 'test']

    def get(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect(reverse('e-403'))
        else:
            return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect(reverse('e-403'))
        else:
            return super().get(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('t:teacherhome')

    def get_context_data(self, **kwargs):
        data = super(QuestionConceptRefAnsUpdate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['concepts'] = ConceptFormSet(self.request.POST, instance=self.object)
            data['refanswers'] = ReferenceAnswerFormSet(self.request.POST, instance=self.object)
        else:
            data['concepts'] = ConceptFormSet(instance=self.object)
            data['refanswers'] = ReferenceAnswerFormSet(instance=self.object)
        return data

    def form_invalid(self, form):
        messages.error(self.request, 'Ocorreu um erro na atualização da questão!')
        return super().form_invalid(form)

    def form_valid(self, form):
        form.instance.user = self.request.user
        context = self.get_context_data()
        concepts = context['concepts']
        refanswers = context['refanswers']
        with transaction.atomic():
            # noinspection PyAttributeOutsideInit
            self.object = form.save()

            if refanswers.is_valid():
                refanswers.instance = self.object
                refanswers.save()
            if concepts.is_valid():
                concepts.instance = self.object
                concepts.save()
            messages.info(self.request, 'O cadastro da questão foi realizado com sucesso!')
        return super(QuestionConceptRefAnsUpdate, self).form_valid(form)


class QuestionByTestListView(LoginRequiredMixin, ListView):
    model = Question

    def get_queryset(self):
        pk = self.kwargs['pk']
        try:
            t = Test.objects.get(pk=pk)
        except ObjectDoesNotExist:
            return Question.objects.none()

        return Question.objects.filter(test=t)


class QuestionDetailView(LoginRequiredMixin, DetailView):
    model = Question


class QuestionDeleteView(LoginRequiredMixin, DeleteView):
    model = Question
    success_url = reverse_lazy('t:teacherhome')

    def get(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect(reverse('e-403'))
        else:
            return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect(reverse('e-403'))
        else:
            return super().get(request, *args, **kwargs)


class TestCreateView(LoginRequiredMixin, CreateView):
    model = Test
    form_class = TestForm

    def get_success_url(self):
        return reverse('t:teacherhome')

    def form_invalid(self, form):
        messages.error(self.request, 'Ocorreu um erro no cadastro da prova!')
        return super().form_invalid(form)

    def form_valid(self, form):
        messages.info(self.request, 'O cadastro da prova foi realizado com sucesso!')
        form.instance.user = self.request.user
        return super(TestCreateView, self).form_valid(form)


class TestUpdateView(LoginRequiredMixin, UpdateView):
    model = Test
    fields = ['name', 'description']

    def get(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect(reverse('e-403'))
        else:
            return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect(reverse('e-403'))
        else:
            return super().get(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('t:test-list')

    def form_invalid(self, form):
        messages.error(self.request, 'Ocorreu um erro na atualização da prova!')
        return super().form_invalid(form)

    def form_valid(self, form):
        messages.info(self.request, 'O cadastro da prova foi realizado com sucesso!')
        form.instance.user = self.request.user
        return super(TestUpdateView, self).form_valid(form)


class TestListView(LoginRequiredMixin, ListView):
    model = Test

    def get_queryset(self):
        return Test.objects.filter(user=self.request.user)


class TestAllListView(LoginRequiredMixin, ListView):
    model = Test

    def get_context_data(self, **kwargs):
        data = super(TestAllListView, self).get_context_data(**kwargs)
        data['is_all'] = True
        return data

    def get_queryset(self):
        return Test.objects.all()


class TestDeleteView(LoginRequiredMixin, DeleteView):
    model = Test
    success_url = reverse_lazy('t:test-list')

    def get(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect(reverse('e-403'))
        else:
            return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return redirect(reverse('e-403'))
        else:
            return super().get(request, *args, **kwargs)


class ConceptCreateView(LoginRequiredMixin, CreateView):
    model = Concept
    fields = ['concept_text']

    def get_success_url(self):
        context = self.get_context_data()
        pk = context['question_pk']
        return reverse('t:question-details', kwargs={'pk': pk})

    def get_context_data(self, **kwargs):
        data = super(ConceptCreateView, self).get_context_data(**kwargs)
        data['question_pk'] = self.kwargs['pk']
        return data

    def form_invalid(self, form):
        messages.error(self.request, 'Ocorreu um erro no cadastro do conceito!')
        return super().form_invalid(form)

    def form_valid(self, form):
        messages.info(self.request, 'O cadastro do conceito foi realizado com sucesso!')
        context = self.get_context_data()
        pk = context['question_pk']
        form.instance.question = Question.objects.get(id=pk)
        return super(ConceptCreateView, self).form_valid(form)


class RefAnsCreateView(LoginRequiredMixin, CreateView):
    model = ReferenceAnswer
    fields = ['ref_ans_text']
    template_name = 't/reference_answer_form.html'

    def get_success_url(self):
        context = self.get_context_data()
        pk = context['question_pk']
        return reverse('t:question-details', kwargs={'pk': pk})

    def get_context_data(self, **kwargs):
        data = super(RefAnsCreateView, self).get_context_data(**kwargs)
        data['question_pk'] = self.kwargs['pk']
        return data

    def form_invalid(self, form):
        messages.error(self.request, 'Ocorreu um erro no cadastro da resposta de referência!')
        return super().form_invalid(form)

    def form_valid(self, form):
        messages.info(self.request, 'O cadastro da resposta de referência foi realizado com sucesso!')
        context = self.get_context_data()
        pk = context['question_pk']
        form.instance.question = Question.objects.get(id=pk)
        return super(RefAnsCreateView, self).form_valid(form)


@login_required
def question_answersgrading(request, pk):
    if request.method == 'GET':
        form = forms.GradeAnswerForm()
        q = Question.objects.get(pk=pk)
        if request.user.id == q.user.id:
            answers_for_question_x = list(Answer.objects.filter(question__pk=pk, t_bin_grade__isnull=True))
        else:
            grades_i_already_gave_answers_ids = Grade.objects.filter(grader=request.user).values('answer_id')
            answers_for_question_x = list(Answer.objects.filter(
                Q(question__pk=pk),
                ~Q(id__in=grades_i_already_gave_answers_ids)
            ))
        if answers_for_question_x:
            answers_for_question_x_dict_list = []
            concepts = []
            ref_ans = []
            for a in answers_for_question_x:
                if str(a.question.id) == str(pk):
                    concepts.extend(a.question.concepts.all())
                    ref_ans.extend(a.question.ref_answers.all())
                    break
            for a in answers_for_question_x:
                aa = {'answer_text': a.answer_text,
                      'question_statement': a.question.statement,
                      'question_id': a.question.id,
                      'question_creator_id': a.question.user.id,
                      'answer_id': a.id}
                answers_for_question_x_dict_list.append(aa)

            request.session['answers_for_question_x_dict_list'] = answers_for_question_x_dict_list
            return render(request, 't/grade_answer.html', {'form': form, 'concepts': concepts, 'ref_ans': ref_ans})
        else:
            messages.info(request, "Não há respostas a serem avaliadas para essa questão")
            return redirect(reverse('t:teacherhome'))
    elif request.method == 'POST':
        form = forms.GradeAnswerForm(request.POST)
        if form.is_valid():
            answers_for_question_x_dict_list = request.session['answers_for_question_x_dict_list']
            curr_ans = answers_for_question_x_dict_list.pop(0)

            a = Answer.objects.get(pk=curr_ans['answer_id'])
            if request.user.id == curr_ans['question_creator_id']:
                a.t_bin_grade = form.cleaned_data.get('t_bin_grade', 'erro')
                a.t_grade = form.cleaned_data.get('t_grade', 'erro')
                a.save()
            else:
                grade = Grade()
                grade.answer = a
                grade.grader = request.user
                grade.ot_grade = form.cleaned_data.get('t_grade', 'erro')
                grade.ot_bin_grade = form.cleaned_data.get('t_bin_grade', 'erro')
                grade.save()

            request.session['answers_for_question_x_dict_list'] = answers_for_question_x_dict_list
            messages.info(request, 'A atribuição da nota foi realizada com sucesso!')
            if 'continuar' in request.POST:
                return redirect(reverse('t:question-answersgrading', kwargs={'pk': curr_ans['question_id']}))
            elif 'terminar' in request.POST:
                return redirect(reverse('t:teacherhome'))
        else:
            messages.error(request, "Ocorreu um erro na atribuição da nota!")
            return redirect(reverse('t:teacherhome'))


class AnswerListView(LoginRequiredMixin, ListView):
    model = Answer

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Answer.objects.filter(question__pk=pk)
