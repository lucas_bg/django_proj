from django.apps import AppConfig


class TConfig(AppConfig):
    name = 'proj.t'
    verbose_name = "Teacher"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        pass
