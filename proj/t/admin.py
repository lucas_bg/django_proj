from django.contrib import admin

from . import models


@admin.register(models.Answer)
class AnswerAdmin(admin.ModelAdmin):

    fields = ['user', 'question', 'answer_text', 't_grade', 't_bin_grade', 'a_grade', 'a_bin_grade']
    search_fields = ['user', 'question']
    list_filter = ['question']
    list_display = ['id', 'user']
    list_editable = []


@admin.register(models.Concept)
class ConceptAdmin(admin.ModelAdmin):

    fields = ['question', 'concept_text']
    search_fields = ['question__statement', 'concept_text']
    list_filter = ['question']
    list_display = ['id', 'question', 'concept_text']
    list_editable = ['concept_text']


@admin.register(models.Grade)
class GradeAdmin(admin.ModelAdmin):

    fields = ['grader', 'answer', 'ot_grade', 'ot_bin_grade']
    search_fields = ['grader']
    list_filter = ['grader']
    list_display = ['id', 'grader', 'ot_grade', 'ot_bin_grade']
    list_editable = []


@admin.register(models.Question)
class QuestionAdmin(admin.ModelAdmin):

    fields = ['user', 'test', 'statement']
    search_fields = ['user__username', 'test__name', 'statement']
    list_filter = ['test']
    list_display = ['id', 'user', 'test']
    list_editable = []


@admin.register(models.ReferenceAnswer)
class ReferenceAnswerAdmin(admin.ModelAdmin):

    fields = ['question', 'ref_ans_text']
    search_fields = ['question__statement', 'ref_ans_text']
    list_filter = ['question']
    list_display = ['id', 'question']
    list_editable = []


@admin.register(models.Test)
class TestAdmin(admin.ModelAdmin):

    fields = ['user', 'name', 'description']
    search_fields = ['user', 'name', 'description']
    list_filter = []
    list_display = ['id', 'user', 'name']
    list_editable = ['name']
