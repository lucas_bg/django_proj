from django.forms import ModelForm, inlineformset_factory
from django import forms
from django.forms.utils import flatatt
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from proj.t.models import Concept, Question, ReferenceAnswer, Test, Answer


class QuestionForm(ModelForm):

    class Meta:
        model = Question
        fields = ['test', 'statement']
        widgets = {
            'statement': forms.Textarea(attrs={'rows': 4})
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(QuestionForm, self).__init__(*args, **kwargs)
        self.fields['test'].queryset = Test.objects.filter(user=self.user)


class ConceptForm(ModelForm):
    class Meta:
        model = Concept
        exclude = ()
        labels = {
            'concept_text': 'Conceito',
        }
        widgets = {
            'concept_text': forms.TextInput(attrs={'class': "textinput textInput form-control"})
        }


ConceptFormSet = inlineformset_factory(Question, Concept, form=ConceptForm, extra=1)


class ReferenceAnswerForm(ModelForm):
    class Meta:
        model = ReferenceAnswer
        exclude = ()
        labels = {
            'ref_ans_text': 'Resposta de Referência',
        }
        widgets = {
            'ref_ans_text': forms.Textarea(attrs={'class': "textarea form-control", 'rows': 5})
        }


ReferenceAnswerFormSet = inlineformset_factory(Question, ReferenceAnswer, form=ReferenceAnswerForm, extra=1)


class CustomSelect(forms.Select):
    def render(self, name, value, attrs=None):
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, name=name)
        output = [format_html('<select{}>', flatatt(final_attrs))]
        output.append('<option value="" selected disabled>-------</option>')
        options = self.render_options([value])
        if options:
            output.append(options)
        output.append('</select>')
        return mark_safe('\n'.join(output))

    def render_option(self, selected_choices, option_value, option_label):
        if option_value is None:
            option_value = ''
        option_value = force_text(option_value)
        selected_html = ''
        return format_html('<option value="{}"{}>{}</option>', option_value, selected_html, force_text(option_label))


class GradeAnswerForm(forms.Form):
    CHOICES = [(True, 'Correto'),
               (False, 'Incorreto')]
    t_bin_grade = forms.ChoiceField(choices=CHOICES, label='Nota Binária', widget=forms.Select())
    t_grade = forms.ChoiceField(choices=Answer.GRADE_CHOICES, label='Nota', widget=CustomSelect())


class TestForm(ModelForm):
    class Meta:
        model = Test
        fields = ['name', 'description']
        widgets = {
            'description': forms.Textarea(attrs={'rows': 4})
        }
