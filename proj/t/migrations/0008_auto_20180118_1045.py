# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2018-01-18 12:45
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('t', '0007_auto_20180117_1605'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='my_answers', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='concept',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='concepts', to='t.Question'),
        ),
        migrations.AlterField(
            model_name='question',
            name='test',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='questions', to='t.Test', verbose_name='Prova'),
        ),
        migrations.AlterField(
            model_name='question',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='my_questions', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='referenceanswer',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ref_answers', to='t.Question'),
        ),
        migrations.AlterField(
            model_name='test',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='my_tests', to=settings.AUTH_USER_MODEL),
        ),
    ]
