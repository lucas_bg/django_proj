import pickle
from collections import defaultdict

import numpy as np

from proj.autograde import extract_all as eall
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.forms import formset_factory
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import ListView, CreateView

from proj.s.forms import AnswerForm
from proj.t.models import Question, Answer, Test, ReferenceAnswer
from proj.s import forms


def convert_to_binary(num):
    if num == 0 or num == 1:
        return 0
    elif num == 2 or num == 3:
        return 1


class QuestionsListView(LoginRequiredMixin, ListView):
    model = Question
    template_name = 's/question_list.html'

    def get_queryset(self):
        return Question.objects.filter(~Q(answers__user=self.request.user))


class AnswerCreateView(LoginRequiredMixin, CreateView):
    model = Answer
    fields = ['answer_text']
    template_name = 's/answer_form.html'

    def get_success_url(self):
        return reverse('s:question-list-all')

    def get_context_data(self, **kwargs):
        context = super(AnswerCreateView, self).get_context_data(**kwargs)
        context['question'] = Question.objects.get(pk=self.kwargs['pk'])
        return context

    def form_invalid(self, form):
        messages.error(self.request, 'Ocorreu um erro no cadastro da resposta!')
        return super().form_invalid(form)

    def form_valid(self, form):
        messages.info(self.request, 'O cadastro da resposta foi realizado com sucesso!')
        form.instance.user = self.request.user
        pkk = self.kwargs['pk']
        form.instance.question = Question.objects.get(pk=pkk)
        # --------------------------------------------------------------------------
        try:
            ref_ans_list = ReferenceAnswer.objects.filter(question_id=pkk)
            ras_dict = defaultdict(list)
            for ra in ref_ans_list:
                ras_dict[pkk].append(ra.ref_ans_text)
            spc_ref_anss = ras_dict[pkk]

            ans_text = form.cleaned_data['answer_text']

            clf = pickle.load(open("proj/autograde/models/m_" + str(pkk) + ".pkl", 'rb'))
            cv1 = pickle.load(open("proj/autograde/models/cv1_" + str(pkk) + ".pkl", 'rb'))
            cv2 = pickle.load(open("proj/autograde/models/cv2_" + str(pkk) + ".pkl", 'rb'))

            X_lexsim = eall.get_lex_feats(ans_text, spc_ref_anss)
            xboww = eall.get_ngrams(cv1, cv2, [ans_text])
            X_tslda = eall.get_ts_lda_feats([ans_text], xboww)
            X_wnsim = eall.get_wn_feats(ans_text, spc_ref_anss)

            X = np.append(xboww, X_lexsim)
            X = np.append(X, X_wnsim)
            X = np.append(X, X_tslda)
            X = np.array([X])
            pred = clf.predict(X)

            form.instance.a_grade = pred[0]
            form.instance.a_bin_grade = convert_to_binary(pred[0])
        except FileNotFoundError:
            print("FILE NOT FOUND, MODEL NOT YET TRAINED")
        # --------------------------------------------------------------------------
        return super().form_valid(form)


class TestListView(LoginRequiredMixin, ListView):
    model = Test
    template_name = 's/test_list.html'


class AnswerListView(LoginRequiredMixin, ListView):
    model = Answer
    template_name = 's/answer_list.html'

    def get_queryset(self):
        return Answer.objects.filter(user=self.request.user)


def bin_mean(bin_list, teacher_bin):
    if len(bin_list) == 0:
        return None
    if len(bin_list) % 2 == 0:
        bin_list.append(teacher_bin)
    return max(set(bin_list), key=bin_list.count)


@login_required
def answer_list_view(request):
    if request.method == 'GET':
        answers = Answer.objects.filter(user=request.user)
        object_list = []
        for a in answers:
            other_grades = a.grades.all()
            if len(other_grades) > 0:
                media = 0
                bin_ot_grades = [og.ot_bin_grade for og in other_grades]
                for og in other_grades:
                    media += og.ot_grade
                media /= len(other_grades)
                if a.t_grade is not None:
                    mean_overall = (media * len(other_grades) + a.t_grade) / (1 + len(other_grades))
                else:
                    mean_overall = None
            else:
                bin_ot_grades = []
                media = None
                mean_overall = None
            new_ans = {
                'mean_overall': mean_overall,
                'mean_other_teachers': media,
                'mean_bin_overall': bin_mean(bin_ot_grades, a.t_bin_grade),
                'question_statement': a.question.statement,
                'answer_text': a.answer_text,
                't_grade': a.t_grade,
                't_bin_grade': a.t_bin_grade,
                'a_grade': a.a_grade,
                'a_bin_grade': a.a_bin_grade,
            }
            object_list.append(new_ans)
        return render(request, 's/answer_list.html', {'object_list': object_list})


@login_required
def search_test_and_question_by_teacher_name_or_id(request):
    if request.method == 'POST':
        if 'test' in request.POST:
            form = forms.SearchTestOrQuestionByTeacherNameOrId(request.POST)
            if form.is_valid():
                if form.cleaned_data['idd'] is None:
                    form.cleaned_data['idd'] = ''
                if form.cleaned_data['idd'] == '' and form.cleaned_data['nome'] == '':
                    tests = Test.objects.none()
                elif form.cleaned_data['idd'] != '' and form.cleaned_data['nome'] == '':
                    tests = Test.objects.filter(user__pk=form.cleaned_data['idd'])
                elif form.cleaned_data['idd'] == '' and form.cleaned_data['nome'] != '':
                    tests = Test.objects.filter(user__ass_teacher__nome__icontains=form.cleaned_data['nome'])
                elif form.cleaned_data['idd'] != '' and form.cleaned_data['nome'] != '':
                    tests = Test.objects.filter(
                        Q(user__pk=form.cleaned_data['idd']),
                        Q(user__ass_teacher__nome__icontains=form.cleaned_data['nome'])
                    )
                else:
                    tests = Test.objects.none()
            else:
                tests = Test.objects.none()
            return render(request, 's/test_list.html', {'object_list': tests})
        elif 'question':
            form = forms.SearchTestOrQuestionByTeacherNameOrId(request.POST)
            if form.is_valid():
                if form.cleaned_data['idd'] is None:
                    form.cleaned_data['idd'] = ''
                if form.cleaned_data['idd'] == '' and form.cleaned_data['nome'] == '':
                    questions = Question.objects.none()
                elif form.cleaned_data['idd'] != '' and form.cleaned_data['nome'] == '':
                    questions = Question.objects.filter(
                        ~Q(answers__user=request.user),
                        Q(user__pk=form.cleaned_data['idd'])
                    )
                elif form.cleaned_data['idd'] == '' and form.cleaned_data['nome'] != '':
                    questions = Question.objects.filter(
                        ~Q(answers__user=request.user),
                        Q(user__ass_teacher__nome__icontains=form.cleaned_data['nome'])
                    )
                elif form.cleaned_data['idd'] != '' and form.cleaned_data['nome'] != '':
                    questions = Question.objects.filter(
                        ~Q(answers__user=request.user),
                        Q(user__pk=form.cleaned_data['idd']),
                        Q(user__ass_teacher__nome__icontains=form.cleaned_data['nome'])
                    )
                else:
                    questions = Question.objects.none()
            else:
                questions = Question.objects.none()
            return render(request, 's/question_list.html', {'object_list': questions})


@login_required
def index_view(request):
    form = forms.SearchTestOrQuestionByTeacherNameOrId()
    return render(request, 's/student_index.html', {'form': form})


@login_required
def test_x(request, pk):
    AnswerFormSet = formset_factory(AnswerForm, extra=1)

    if request.method == 'GET':
        questions = Question.objects.filter(
            ~Q(answers__user=request.user),
            Q(test_id=pk)
        )
        questions_id_list = [{'question_iddd': q.id} for q in questions]
        test_formset = AnswerFormSet(initial=questions_id_list)

        return render(request, 's/test.html', {
            'test_formset': test_formset,
            'questions': questions
        })
    elif request.method == 'POST':
        test_formset = AnswerFormSet(request.POST)

        questions = Question.objects.filter(
            ~Q(answers__user=request.user),
            Q(test_id=pk)
        )
        answers_list = []

        if test_formset.is_valid():
            for f in test_formset[0:len(test_formset)-1]:
                new_answer = Answer()
                new_answer.answer_text = f.cleaned_data.get('answer_text', 'erro')
                for q in questions:
                    if q.id == f.cleaned_data.get('question_iddd', 'erro'):
                        new_answer.question = q
                new_answer.user = request.user
                if new_answer.answer_text != '':
                    answers_list.append(new_answer)
            Answer.objects.bulk_create(answers_list)
            messages.info(request, 'Suas respostas para a prova foram cadastradas com sucesso!')
            return redirect(reverse('s:test-list-all'))
        else:
            messages.error(request, "Ocorreu um erro no cadastro das suas respostas! "
                                    "Talvez você tenha deixado alguma questão em branco?")
            return redirect(reverse('s:studenthome'))
