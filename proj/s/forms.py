from django import forms
from django.forms import ModelForm

from proj.t.models import Answer


class SearchTestOrQuestionByTeacherNameOrId(forms.Form):
    nome = forms.CharField(required=False, label='Nome do Professor')
    idd = forms.IntegerField(required=False, label='Id do Professor')


class AnswerForm(ModelForm):
    question_iddd = forms.IntegerField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(AnswerForm, self).__init__(*args, **kwargs)

        for key in self.fields:
            self.fields[key].required = True

    class Meta:
        model = Answer
        fields = ['answer_text']
        widgets = {
            'answer_text': forms.Textarea(attrs={'class': "textarea form-control", 'rows': 5})
        }
