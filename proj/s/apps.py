from django.apps import AppConfig


class SConfig(AppConfig):
    name = 'proj.s'
    verbose_name = "Student"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        pass
