from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        # /s/
        regex=r'^$',
        view=views.index_view,
        name='studenthome'
    ),
    url(
        # /s/question/list/all/
        regex=r'^question/list/all/$',
        view=views.QuestionsListView.as_view(),
        name='question-list-all'
    ),
    url(
        # /s/answer/createforquestion/(?P<pk>[0-9]+)/
        regex=r'^answer/createforquestion/(?P<pk>[0-9]+)/$',
        view=views.AnswerCreateView.as_view(),
        name='answer-createforquestion'
    ),
    url(
        # /s/test/list/all/
        regex=r'^test/list/all/$',
        view=views.TestListView.as_view(),
        name='test-list-all'
    ),
    url(
        # /s/answer/list/my/
        regex=r'^answer/list/my/$',
        view=views.answer_list_view,
        name='answer-list-my'
    ),
    url(
        # /s/testandquestion/search/tnameorid/
        regex=r'^testandquestion/search/tnameorid/$',
        view=views.search_test_and_question_by_teacher_name_or_id,
        name='test-and-question-search-by-teachernameorid'
    ),
    url(
        # /s/test/(?P<pk>[0-9]+)/
        regex=r'^test/(?P<pk>[0-9]+)/$',
        view=views.test_x,
        name='test-x'
    ),
]
