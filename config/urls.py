from django.conf import settings
from django.conf.urls import include, url, handler400, handler403, handler404, handler500
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views
from django.views.generic import TemplateView

from proj.users import views

urlpatterns = [
                  url(r'^$', views.home_view, name='home'),
                  url(r'^about/$', TemplateView.as_view(template_name='pages/about.html'), name='about'),
                  url(r'^contact/$', TemplateView.as_view(template_name='pages/contact.html'), name='contact'),

                  # Django Admin, use {% url 'admin:index' %}
                  url(settings.ADMIN_URL, admin.site.urls),

                  url(r'^users/', include('proj.users.urls', namespace='users')),
                  url(r'^accounts/', include('allauth.urls')),

                  url(r'^t/', include('proj.t.urls', namespace='t')),
                  url(r'^s/', include('proj.s.urls', namespace='s')),
                  url(r'^error/$', views.error_view, name='error-page'),
                  url(r'^info/$', views.info_view, name='info-page'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += [
    url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}, name='e-400'),
    url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}, name='e-403'),
    url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}, name='e-404'),
    url(r'^500/$', default_views.server_error, name='e-500'),
]

handler400 = views.error_400
handler403 = views.error_403
handler404 = views.error_404
handler500 = views.error_500

if settings.DEBUG:
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [
                          url(r'^__debug__/', include(debug_toolbar.urls)),
                      ] + urlpatterns
