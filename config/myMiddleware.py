from django.contrib import messages
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.urls import reverse
from django.conf import settings


class SimpleMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # ----------------- BEFORE THE VIEW --------------------
        if request.user.is_authenticated:
            u = request.user
            path = request.path
            if u.is_superuser:
                return self.get_response(request)
            if u.finished_signup and u.requestForProfessor and not u.isProfessor:
                logout(request)
                messages.warning(request, 'Seu cadastro foi realizado com sucesso, porém seu acesso ainda nao foi '
                                          'autorizado pelo administrador.')
                return redirect(reverse('info-page'))
            elif not u.finished_signup and path == '/accounts/logout/':
                logout(request)
                return redirect(reverse('home'))
            elif not u.finished_signup and path != '/users/~chooseusertype/'\
            and path != '/users/~studentsignup/' and path != '/users/~teachersignup/':
                return redirect('/users/~chooseusertype/')
            elif u.isProfessor and '/s/' in path:
                messages.error(request, 'Acesso Proibido, volte à pagina inicial.')
                return redirect(reverse('info-page'))
            elif not u.isProfessor and '/t/' in path:
                messages.error(request, 'Acesso Proibido, volte à pagina inicial.')
                return redirect(reverse('info-page'))
        else:
            if request.path == '/':
                pass
            else:
                allowed_urls_when_not_logged_in = [
                    '/about/',
                    '/contact/',
                    '/accounts/',
                    '/admin/',
                    '/admiiin/',
                    '/error/',
                    '/info/',
                    '/400/',
                    '/403/',
                    '/404/',
                    '/500/'
                ]
                vet = [False for item in range(len(allowed_urls_when_not_logged_in))]
                for i, pattern in enumerate(allowed_urls_when_not_logged_in):
                    vet[i] = request.path.startswith(pattern)
                if not any(vet):
                    messages.error(request, 'Acesso Proibido, volte a pagina inicial.')
                    return redirect(reverse('info-page'))
        # ----------------- BEFORE THE VIEW --------------------
        response = self.get_response(request)
        # ----------------- AFTER THE VIEW --------------------

        # ----------------- AFTER THE VIEW --------------------
        return response

    if not settings.DEBUG:
        def process_exception(self, request, exception):
            messages.error(request, 'Um erro desconhecido ocorreu.')
            if hasattr(exception, 'message'):
                print('%s' % exception.message)
            print('(%s)' % (type(exception)))
            return redirect(reverse('info-page'))
