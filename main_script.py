import django
django.setup()

import pickle
from collections import defaultdict
import numpy as np

from proj.autograde import extract_all as eall
from proj.t.models import Answer, ReferenceAnswer


def convert_to_binary(num):
    if num == 0 or num == 1:
        return 0
    elif num == 2 or num == 3:
        return 1


answer_list = Answer.objects.filter(a_grade__isnull=True, question_id__gte=36)
ref_ans_list = ReferenceAnswer.objects.filter(question_id__gte=36)

ras_dict = defaultdict(list)
for ra in ref_ans_list:
    ras_dict[ra.question_id].append(ra.ref_ans_text)

for a in answer_list:
    spc_ref_anss = ras_dict[a.question_id]
    print(a.id)
    ans_text = a.answer_text

    clf = pickle.load(open("proj/autograde/models/m_" + str(a.question_id) + ".pkl", 'rb'))
    cv1 = pickle.load(open("proj/autograde/models/cv1_" + str(a.question_id) + ".pkl", 'rb'))
    cv2 = pickle.load(open("proj/autograde/models/cv2_" + str(a.question_id) + ".pkl", 'rb'))

    X_lexsim = eall.get_lex_feats(ans_text, spc_ref_anss)
    xboww = eall.get_ngrams(cv1, cv2, [ans_text])
    X_tslda = eall.get_ts_lda_feats([ans_text], xboww)
    X_wnsim = eall.get_wn_feats(ans_text, spc_ref_anss)

    X = np.append(xboww, X_lexsim)
    X = np.append(X, X_wnsim)
    X = np.append(X, X_tslda)
    X = np.array([X])
    pred = clf.predict(X)

    a.a_grade = pred[0]
    a.a_bin_grade = convert_to_binary(pred[0])
    a.save()
